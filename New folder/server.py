
from flask import make_response, Flask, json, request, jsonify
from flask_restful import Api, Resource , reqparse
from flask import render_template
from flask_cors import CORS, cross_origin
from naive_bayes import prediction
app = Flask(__name__)
CORS(app)
api = Api(app)

@app.route('/', methods=['GET'])   
def my_json():
   if request.method=='GET':
        # resourc = request.args.get('resourc')
        tem1 = request.args.get('tem1')
        tem2 = request.args.get('tem2')
        tem3 = request.args.get('tem3')
        tem4 = request.args.get('tem4')
        tem5 = request.args.get('tem5')
        tem6 = request.args.get('tem6')
        resource = [int(tem1),int(tem2),int(tem3),int(tem4),int(tem5),int(tem6)]
        print(resource)        
        predicted = prediction(resource)
        data= {
            "status" : 'Success',
            "result" : predicted[0]
        }
        return jsonify(data)

app.run(debug=True)
# --------------------------------------------------------
# class post_dog(Resource):
#     def post(self):
#         # args = parser.parse_args()
#         print('this is args')
#         Temperment_1 = request.form['Temperment1']
#         Temperment_2 = request.form['Temperment2']
#         Temperment_3 = request.form['Temperment3']
#         Temperment_4 = request.form['Temperment4']
#         Temperment_5 = request.form['Temperment5']
#         Temperment_6 = request.form['Temperment6']
        
#         data = {
#             str(prediction([str(Temperment_1),str(Temperment_2),str(Temperment_3),
#             str(Temperment_4),str(Temperment_5),str(Temperment_6)]) # EX: date => 1-1-2018, t => hotel
#             )}

#         response = app.response_class(
#     		response=json.dumps(data),
# 		    mimetype='application/json'
#         )
    
#         return response

# class dog_prediction(Resource):
#     def get(self):
#         print("is called")
#         data = {
# 			'data': str(predicted)
# 		}

#         response = app.response_class(
#     		response=json.dumps(data),
# 		    mimetype='application/json'
#     	)
#         return response

# api.add_resource(dog_prediction, '/')
if __name__ == '__main__':
   app.run(host='127.0.0.1', debug=True, port=5000)
