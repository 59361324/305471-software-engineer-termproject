import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB

data = pd.read_csv('dog_breed_new.csv')

d = {'Active': 1,'Aloof':2,'Alert':3,'Companionable':4,'Agile':5,'Energetic':6,'Independent':7,'EvenTempered':8,
'Bold':9,'Confident':10,'Loyal':11,'Faithful':12,'Calm':13,'Cheerful':14,'Fearless':15,'Intelligent':16,'Courageous':17,
'Devoted':18,'Dignified':19,'Docile':20,'Gentle':21,'Friendly':22,'Happy':23,'Protective':24,' Strong-willed':25,'Trainable':26,
'Affectionate':27,'Nan':44 }
data.Temperment1 = [d[item] for item in data.Temperment1.astype(str)]

e = {'Active': 1,'Aloof':2,'Alert':3,'Companionable':4,'Agile':5,'Energetic':6,'Independent':7,'EvenTempered':8,
'Bold':9,'Confident':10,'Loyal':11,'Faithful':12,'Calm':13,'Cheerful':14,'Fearless':15,'Intelligent':16,'Courageous':17,
'Devoted':18,'Dignified':19,'Docile':20,'Gentle':21,'Friendly':22,'Happy':23,'Protective':24,' Strong-willed':25,'Trainable':26,
'Affectionate':27,'Hardy':28,'Lively':29,'Quiet':30,'Spirited':31,'Outgoing':32,'Loving':33,'Obedient':34,'Playful':35,'Nan':44 }
data.Temperment2 = [e[item] for item in data.Temperment2.astype(str)]

f = {'Active': 1,'Aloof':2,'Alert':3,'Companionable':4,'Agile':5,'Energetic':6,'Independent':7,'EvenTempered':8,
'Bold':9,'Confident':10,'Loyal':11,'Faithful':12,'Calm':13,'Cheerful':14,'Fearless':15,'Intelligent':16,'Courageous':17,
'Devoted':18,'Dignified':19,'Docile':20,'Gentle':21,'Friendly':22,'Happy':23,'Protective':24,' Strong-willed':25,'Trainable':26,
'Affectionate':27,'Hardy':28,'Lively':29,'Quiet':30,'Spirited':31,'Outgoing':32,'Loving':33,'Obedient':34,'Playful':35,'Sensitive':36,
'Quick':37,'Reserverd':38,'Stubborn':39,'Tenacious':40,'Nan':44}
data.Temperment3 = [f[item] for item in data.Temperment3.astype(str)]

g = {'Active': 1,'Aloof':2,'Alert':3,'Companionable':4,'Agile':5,'Energetic':6,'Independent':7,'EvenTempered':8,
'Bold':9,'Confident':10,'Loyal':11,'Faithful':12,'Calm':13,'Cheerful':14,'Fearless':15,'Intelligent':16,'Courageous':17,
'Devoted':18,'Dignified':19,'Docile':20,'Gentle':21,'Friendly':22,'Happy':23,'Protective':24,' Strong-willed':25,'Trainable':26,
'Affectionate':27,'Hardy':28,'Lively':29,'Quiet':30,'Spirited':31,'Outgoing':32,'Loving':33,'Obedient':34,'Playful':35,'Sensitive':36,
'Quick':37,'Reserverd':38,'Stubborn':39,'Tenacious':40,'Sweet-tempered':41,'Sociable':42,'Responsive':43,'Nan':44}
data.Temperment4 = [g[item] for item in data.Temperment4.astype(str)]

h = {'Active': 1,'Aloof':2,'Alert':3,'Companionable':4,'Agile':5,'Energetic':6,'Independent':7,'EvenTempered':8,
'Bold':9,'Confident':10,'Loyal':11,'Faithful':12,'Calm':13,'Cheerful':14,'Fearless':15,'Intelligent':16,'Courageous':17,
'Devoted':18,'Dignified':19,'Docile':20,'Gentle':21,'Friendly':22,'Happy':23,'Protective':24,' Strong-willed':25,'Trainable':26,
'Affectionate':27,'Hardy':28,'Lively':29,'Quiet':30,'Spirited':31,'Outgoing':32,'Loving':33,'Obedient':34,'Playful':35,'Sensitive':36,
'Quick':37,'Reserverd':38,'Stubborn':39,'Tenacious':40,'Sweet-tempered':41,'Sociable':42,'Responsive':43,'Nan':44}
data.Temperment5 = [h[item] for item in data.Temperment5.astype(str)]

i = {'Active': 1,'Aloof':2,'Alert':3,'Companionable':4,'Agile':5,'Energetic':6,'Independent':7,'EvenTempered':8,
'Bold':9,'Confident':10,'Loyal':11,'Faithful':12,'Calm':13,'Cheerful':14,'Fearless':15,'Intelligent':16,'Courageous':17,
'Devoted':18,'Dignified':19,'Docile':20,'Gentle':21,'Friendly':22,'Happy':23,'Protective':24,' Strong-willed':25,'Trainable':26,
'Affectionate':27,'Hardy':28,'Lively':29,'Quiet':30,'Spirited':31,'Outgoing':32,'Loving':33,'Obedient':34,'Playful':35,'Sensitive':36,
'Quick':37,'Reserverd':38,'Stubborn':39,'Tenacious':40,'Sweet-tempered':41,'Sociable':42,'Responsive':43,'Nan':44}
data.Temperment6 = [i[item] for item in data.Temperment6.astype(str)]

A = pd.DataFrame(np.c_[data['Temperment1'], data['Temperment2'], data['Temperment3'], data['Temperment4'],data['Temperment5'],data['Temperment6']],
columns = ['Temperment1','Temperment2','Temperment3','Temperment4','Temperment5','Temperment6'])
C = data['BreedName']


A_train, A_test, C_train, C_test = train_test_split(A, C, test_size = 0.5, random_state=6)
model = GaussianNB().fit(A_train, C_train)

def prediction(Resourc):
    predicted = model.predict([Resourc])
    return predicted

print(prediction)

