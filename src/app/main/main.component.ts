import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams} from '@angular/common/http';
import { Variable } from '@angular/compiler/src/render3/r3_ast';

export interface Temp_name {
  value: number;
  viewValue: string;
}
export interface temperment{
  x : Variable;
}


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})


export class MainComponent implements OnInit {

result : string ;

  temp1: string;
  temp2: string;
  temp3: string;
  temp4: string;
  temp5: string;
  temp6: string;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.openSnackBar()
  }
  temps: Temp_name[] = [
    { value: 44, viewValue: 'Nan' },
    { value: 1, viewValue: 'Active' },
    { value: 2, viewValue: 'Aloof' },
    { value: 3, viewValue: 'Alert' },
    { value: 4, viewValue: 'Companionable' },
    { value: 5, viewValue: 'Agile' },
    { value: 6, viewValue: 'Energetic' },
    { value: 7, viewValue: 'Independent' },
    { value: 8, viewValue: 'EvenTempered' },
    { value: 9, viewValue: 'Boil' },
    { value: 10, viewValue: 'Confident' },
    { value: 11, viewValue: 'Loyal' },
    { value: 12, viewValue: 'Frithful' },
    { value: 13, viewValue: 'Calm' },
    { value: 14, viewValue: 'Cheerful' },
    { value: 15, viewValue: 'Fearless' },
    { value: 16, viewValue: 'Intelligent' },
    { value: 17, viewValue: 'Courageous' },
    { value: 18, viewValue: 'Devoted' },
    { value: 19, viewValue: 'Dignified' },
    { value: 20, viewValue: 'Docile' },
    { value: 21, viewValue: 'Gentle' },
    { value: 22, viewValue: 'Friendly' },
    { value: 23, viewValue: 'Happy' },
    { value: 24, viewValue: 'Protective' },
    { value: 25, viewValue: 'Strong-willed' },
    { value: 26, viewValue: 'Trainable' },
    { value: 27, viewValue: 'Affectionate' },
    { value: 28, viewValue: 'Hardy' },
    { value: 29, viewValue: 'Lively' },
    { value: 30, viewValue: 'Quiet' },
    { value: 31, viewValue: 'Spirited' },
    { value: 32, viewValue: 'Outgoing' },
    { value: 33, viewValue: 'Loving' },
    { value: 34, viewValue: 'Obedient' },
    { value: 35, viewValue: 'Playful' },
    { value: 36, viewValue: 'Sensitive' },
    { value: 37, viewValue: 'Quick' },
    { value: 38, viewValue: 'Reserverd' },
    { value: 39, viewValue: 'Stubborn' },
    { value: 40, viewValue: 'Tenacious' },
    { value: 41, viewValue: 'Sweet-tempered' },
    { value: 42, viewValue: 'Sociable' },
    { value: 43, viewValue: 'Responsive' }
  ];

  url: string = "http://127.0.0.1:4300/?tem1="+this.temp1+"&tem2="+this.temp2+"&tem3="+this.temp3+"&tem4="+this.temp4+"&tem5="+this.temp5+"&tem6="+this.temp6;
  openSnackBar() {
    // var temp1 = (<HTMLInputElement>document.getElementById('t1')).value;
    // var temp2 = (<HTMLInputElement>document.getElementById('t2')).value;
    // var temp3 = (<HTMLInputElement>document.getElementById('t3')).value;
    // var temp4 = (<HTMLInputElement>document.getElementById('t4')).value;
    // var temp5 = (<HTMLInputElement>document.getElementById('t5')).value;
    // var temp6 = (<HTMLInputElement>document.getElementById('t6')).value;

    var temp1 = (<HTMLInputElement>document.getElementById('t1')).value;
    var temp2 = (<HTMLInputElement>document.getElementById('t2')).value;
    var temp3 = (<HTMLInputElement>document.getElementById('t3')).value;
    var temp4 = (<HTMLInputElement>document.getElementById('t4')).value;
    var temp5 = (<HTMLInputElement>document.getElementById('t5')).value;
    var temp6 = (<HTMLInputElement>document.getElementById('t6')).value;
    var urlss = "http://127.0.0.1:5000/?tem1="+temp1+"&tem2="+temp2+"&tem3="+temp3+"&tem4="+temp4+"&tem5="+temp5+"&tem6="+temp6;

    console.log(temp1, temp2, temp3, temp4, temp5, temp6);
    this.http.get<any>(urlss).subscribe(res => {
      this.result = res.result
      console.log(res.result);
    }, err => {
      console.log(err);
    });

    
  }
}
